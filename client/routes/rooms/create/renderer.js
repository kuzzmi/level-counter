import React from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';

import { Redirect } from 'react-router-native';

import Header from '../../../ui-kit/header';
import Input from '../../../ui-kit/input';
import Button from '../../../ui-kit/button';
import BackButton from '../../../ui-kit/backButton';
import ErrorView from '../../../ui-kit/error';

import I18n from '../../../i18n';

export default ({
    name,
    save,

    saving,
    error,

    updateName,

    profile,
}) => (
    !!profile.name ?
        <View style={ styles.page }>
            <View style={ styles.header }>
                <BackButton />
                <Header>
                    { I18n.t('rooms.new.title') }
                </Header>
            </View>
            <View style={ styles.form }>
                <Input
                    value={ name }
                    placeholderCode="rooms.new.name"
                    onChange={ updateName }
                    autoFocus={ true }
                />
            </View>
            <ErrorView namespace="room" error={ error } />
            <View>
                <Button
                    type="primary"
                    titleCode="rooms.new.create"
                    onPress={ save }
                    loading={ saving }
                />
            </View>
        </View> :
        <Redirect to={{
            pathname: '/profile',
            state: {
                redirect: '/rooms/create/',
            },
        }} />
);

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },

    header: {
        flexDirection: 'row',
    },

    form: {
        marginVertical: 10,
        flex: 1,
    },
});
