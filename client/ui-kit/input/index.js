import React, { Component } from 'react';
import {
    View,
    TextInput,
    StyleSheet,
    Animated,
} from 'react-native';

import { COLORS } from '../constants';

import I18n from '../../i18n';

export default class extends Component {
    state = {
        focusedOrNotEmpty: false,
        labelAnim: new Animated.Value(0),
        value: '',
    }

    constructor(props) {
        super(props);

        this.onFocus = this.onFocus.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onChange = this.onChange.bind(this);

        if (!!props.value) {
            this.state.value = props.value;
            this.state.labelAnim = new Animated.Value(1);
        }
    }

    componentWillReceiveProps({ value }) {
        this.setState(state => ({
            ...state,
            value,
            labelAnim: new Animated.Value(1),
        }));
    }

    onFocus() {
        Animated.timing(this.state.labelAnim, {
            toValue: 1,
            duration: 350,
        }).start();
    }

    onBlur() {
        if (this.state.value.length === 0) {
            Animated.timing(this.state.labelAnim, {
                toValue: 0,
                duration: 350,
            }).start();
        }
    }

    onChange(value) {
        this.setState(state => ({
            ...state,
            value,
        }));
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    render() {
        const {
            placeholderCode,
            style,
            type,
        } = this.props;
        return (
            <View style={[ styles.container, style ]}>
                <Animated.Text style={[
                    styles.label,
                    {
                        opacity: this.state.labelAnim.interpolate({
                            inputRange: [0, 1],
                            outputRange: [.8, .7],
                        }),
                        fontSize: this.state.labelAnim.interpolate({
                            inputRange: [0, 1],
                            outputRange: [18, 14],
                        }),
                        transform: [{
                            translateY: this.state.labelAnim.interpolate({
                                inputRange: [0, 1],
                                outputRange: [10, -12],
                            }),
                        }],
                    },
                ]}>
                    { I18n.t(placeholderCode) }
                </Animated.Text>
                <TextInput
                    style={ styles.input }
                    onChangeText={ this.onChange }
                    onFocus={ this.onFocus }
                    onBlur={ this.onBlur }
                    value={ this.state.value }
                    autoFocus={ this.props.autoFocus }
                    underlineColorAndroid="transparent"
                    keyboardType={
                        ['default', 'numeric'].indexOf(type) !== -1 ?
                            type :
                            'default'
                    }
                    secureTextEntry={ type === 'password' }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 12,
    },
    label: {
        fontFamily: 'underdog',
        fontSize: 14,
        color: COLORS.grey,
        position: 'absolute',
    },
    input: {
        height: 40,
        fontFamily: 'underdog',
        fontSize: 18,
        color: COLORS.mustardBg,
        borderBottomColor: COLORS.darkerBrownFg,
        borderBottomWidth: 2,
    },
});
