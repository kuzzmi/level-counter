const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Room = require('./models/room.js');
const cors = require('cors');
const cn = require('./const.js');

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/level-counter', {
    useMongoClient: true,
});

app.use(bodyParser.json());
app.use(cors());

app.use((error, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        error,
    });
});

app.get('/rooms/', (req, res) => {
    const query = req.query.q;

    Room.find({
        name: new RegExp(`.*${query}.*`, 'gi'),
    }).then(rooms => {
        res.json(rooms);
    });
});

app.post('/rooms/', (req, res) => {
    const room = req.body;

    if (!room.fight) {
        room.fight = {
            status: cn.FIGHT_STATUS.INACTIVE,
            fighter: null,
            assistant: null,
            monster: null,
        };
    }

    Room.create(room)
        .then(room => {
            res.json(room);
        }, error => {
            console.log(error);
            res.status(500);
            res.json({
                error,
            });
        });
});

module.exports = app;
