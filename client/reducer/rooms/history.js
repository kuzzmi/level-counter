import Storage from 'client/storage';

export const types = {
    REQUEST: 'ROOMS_HISTORY_LOAD_REQUEST',
    FAILED: 'ROOMS_HISTORY_LOAD_FAILED',
    SUCCESS: 'ROOMS_HISTORY_LOAD_SUCCESS',
};

export const actions = {
    request: () => dispatch => {
        dispatch({
            type: types.REQUEST,
        });

        return Storage.get('history')
            .then(rooms => dispatch(actions.load(rooms)));
    },

    load: data => ({
        type: types.SUCCESS,
        payload: {
            data,
        },
    }),
};

export const initialState = {
    data: null,
    error: false,
    loading: false,
};

const handlers = {
    [types.REQUEST]: state => ({
        ...state,
        data: [],
        loading: true,
        error: false,
    }),

    [types.FAILED]: (state, { error }) => ({
        error,
        data: [],
        loading: false,
    }),

    [types.SUCCESS]: (state, { data }) => ({
        data,
        loading: false,
        error: false,
    }),
};

export default (state = initialState, { type, payload }) => {
    const handler = handlers[type];

    const state_ = handler ? handler(state, payload) : state;

    return state_;
};
