import { Util } from 'expo';
import I18n from 'i18n-js';
import en from './assets/strings/en.json';
import ru from './assets/strings/ru.json';
import uk from './assets/strings/uk.json';

I18n.defaultLocale = 'en-GB';
I18n.fallbacks = true;

I18n.initAsync = async() => {
    let locale = 'en-GB';
    try {
        locale = await Util.getCurrentLocaleAsync();
    } catch (error) {
        /* eslint no-console:0 */
        console.log('Error getting locale: ' + error);
    }

    I18n.locale = locale ? locale.replace(/_/, '-') : '';
};

I18n.translations = {
    en,
    ru,
    uk,
};

export default I18n;
