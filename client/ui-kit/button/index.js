import React from 'react';
import {
    ActivityIndicator,
    TouchableOpacity,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { withRouter } from 'react-router-native';

import { COLORS } from '../constants';

import Icon from '../icon';

import I18n from '../../i18n';

const Button = ({
    title,
    titleCode,
    type = 'default',

    style,
    loading,

    icon,

    linkTo,
    history,

    onPress,
}) => (
    <TouchableOpacity
        onPress={ () => {
            if (loading) {
                return;
            }
            if (!!linkTo) {
                history.push(linkTo);
            } else {
                onPress();
            }
        } }
        activeOpacity={ 0.7 }
        style={[
            styles.button,
            styles[`button-${type}`],
            style,
        ]}>
        {
            loading ?
                <View style={ styles.container }>
                    <ActivityIndicator color={
                        COLORS.mustardBg
                    } />
                </View> :
                <View style={ styles.container }>
                    {
                        icon ?
                            <Icon { ...icon } />
                            : null
                    }
                    {
                        (titleCode || title) &&
                    <Text style={[
                        styles.title,
                        styles[`title-${type}`],
                        {
                            marginLeft: icon ? 15 : 0,
                        },
                    ]}>
                        { titleCode ? I18n.t(titleCode) : title }
                    </Text>
                    }
                </View>
        }
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 14,
        borderRadius: 6,
    },

    'button-default': {
    },

    'button-primary': {
        backgroundColor: COLORS.darkerBrownFg,
    },

    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    title: {
        fontFamily: 'underdog',
        fontSize: 16,
    },

    'title-default': {
        color: COLORS.mustardBg,
    },

    'title-primary': {
        color: COLORS.mustardBg,
    },
});

export default withRouter(Button);
