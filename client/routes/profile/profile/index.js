import { connect } from 'react-redux';
import Renderer from './renderer';
import { actions } from 'client/reducer/profile';

const mapStateToProps = ({ profile }) => profile;

const mapDispatchToProps = actions;

export default connect(mapStateToProps, mapDispatchToProps)(Renderer);
