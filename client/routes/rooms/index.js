import DefaultLayout from '../../layouts/default';

import History from './history';
import Create from './create';
import Search from './search';
import Edit from './edit';
import Play from './play';
import Fight from './battle';

export default [{
    path: '/rooms/history/',
    exact: true,
    component: History,
    layout: DefaultLayout,
    navbar: {
        labelCode: 'rooms',
        exact: false,
        icon: {
            family: 'Feather',
            name: 'list',
        },
    },
}, {
    path: '/rooms/create/',
    exact: true,
    component: Create,
    layout: DefaultLayout,
}, {
    path: '/rooms/search/',
    exact: true,
    component: Search,
    layout: DefaultLayout,
}, {
    path: '/rooms/edit/',
    exact: true,
    component: Edit,
    layout: DefaultLayout,
}, {
    path: '/rooms/fight/',
    exact: true,
    component: Fight,
    layout: DefaultLayout,
}, {
    path: '/rooms/play/',
    exact: true,
    component: Play,
    layout: DefaultLayout,
}];
