const WebSocket = require('ws');
const url = require('url');
const uuid = require('uuid/v1');

const Room = require('./models/room.js');

let wss;

const rooms = {};
const sockets = {};

function processMessage(message) {
    try {
        const { event, payload } = JSON.parse(message);

        switch (event.toLowerCase()) {
            case 'join':
                console.log(`Somebody's joining the room ${payload.roomId}`);
                joinHandler(this, payload);
                break;

            case 'add_new_player':
                console.log('A new player was added');
                addNewPlayerHandler(this, payload);
                break;

            case 'remove_player':
                console.log('Removing a player');
                removePlayerHandler(this, payload);
                break;

            case 'update_room_status':
                console.log('Updating a room status');
                updateRoomStatusHandler(this, payload);
                break;

            case 'update_fight_status':
                console.log('Updating a fight status');
                updateFightStatusHandler(this, payload);
                break;

            case 'next_turn':
                console.log('Updating a room status');
                nextTurnHandler(this, payload);
                break;

            case 'select_player':
                console.log('Updating a currentPlayerIndex');
                selectPlayerHandler(this, payload);
                break;

            case 'update_player':
                console.log('Updating a user parameter');
                updatePlayerHandler(this, payload);
                break;

            case 'update_fighter':
                console.log('Updating a fighter');
                updateFighterHandler(this, payload);
                break;

            case 'update_monster':
                console.log('Updating a monster');
                updateMonsterHandler(this, payload);
                break;

            default:
                return;
        }
    } catch(e) {
        console.error('Sent an unexpected message:', message);
    }
}

function joinHandler(ws, { roomId }) {
    rooms[roomId] = rooms[roomId] || [];
    rooms[roomId].push(ws);
    sockets[ws.id] = roomId;

    Room.findById(roomId).then(room => {
        send(ws, 'CONNECT_TO_ROOM', room)
    });
}

function addNewPlayerHandler(ws, { player }) {
    roomId = sockets[ws.id];

    Room.findById(roomId)
        .then(room => room.addPlayer(player))
        .then(room => {
            const lastPlayer = room.players.pop();
            broadcastToRoom(roomId, 'ADD_NEW_PLAYER', lastPlayer);
        });
}

function removePlayerHandler(ws, { player }) {
    roomId = sockets[ws.id];

    Room.findById(roomId)
        .then(room => room.removePlayer(player))
        .then(room => {
            broadcastToRoom(roomId, 'REMOVE_PLAYER', player._id);
        });
}

function updateRoomStatusHandler(ws, { status }) {
    roomId = sockets[ws.id];

    Room.findById(roomId)
        .then(room => room.updateStatus(status))
        .then(room => {
            broadcastToRoom(roomId, 'UPDATE_ROOM_STATUS', status);
        });
}

function nextTurnHandler(ws) {
    roomId = sockets[ws.id];

    Room.findById(roomId)
        .then(room => room.nextTurn())
        .then(room => {
            broadcastToRoom(roomId, 'UPDATE_TURN', room.currentPlayerIndex);
        });
}

function selectPlayerHandler(ws, { index }) {
    roomId = sockets[ws.id];

    Room.findById(roomId)
        .then(room => {
            room.currentPlayerIndex = index;
            return room.save();
        })
        .then(room => {
            broadcastToRoom(roomId, 'UPDATE_TURN', index);
        });
}

function updatePlayerHandler(ws, { field, value }) {
    roomId = sockets[ws.id];

    Room.findById(roomId)
        .then(room => room.updateCurrentPlayer(field, value))
        .then(player => {
            broadcastToRoom(roomId, 'UPDATE_PLAYER', player);
        });
}

function updateFightStatusHandler(ws, { status }) {
    roomId = sockets[ws.id];

    Room.findById(roomId)
        .then(room => room.updateFight({
            field: 'status',
            value: status,
        }))
        .then(room => {
            broadcastToRoom(roomId, 'UPDATE_FIGHT', room.fight);
        });
}

function updateFighterHandler(ws, { field, value }) {
    roomId = sockets[ws.id];

    Room.findById(roomId)
        .then(room => room.updateFight({
            field: 'fighter',
            subField: field,
            value
        }))
        .then(room => {
            broadcastToRoom(roomId, 'UPDATE_FIGHT', room.fight);
        });
}

function updateMonsterHandler(ws, { field, value }) {
    roomId = sockets[ws.id];

    Room.findById(roomId)
        .then(room => room.updateFight({
            field: 'monster',
            subField: field,
            value
        }))
        .then(room => {
            broadcastToRoom(roomId, 'UPDATE_FIGHT', room.fight);
        });
}

function send(socket, event, payload) {
    socket.send(JSON.stringify({
        event,
        payload,
    }));
}

function broadcast(sockets, event, payload) {
    sockets.forEach(function(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(JSON.stringify({
                event,
                payload,
            }));
        }
    });
}

function broadcastToRoom(roomId, event, payload) {
    broadcast(rooms[roomId], event, payload);
}

function broadcastToAll(event, payload) {
    broadcast(wss.clients, event, payload);
}

function setupListeners(wss) {
    wss.on('connection', function connection(ws, req) {
        const location = url.parse(req.url, true);
        console.log(`connection ${ location.query.uuid }`);

        ws.id = location.query.uuid;

        ws.on('message', processMessage);

        ws.on('close', function() {
            console.log('Connection closed');
        });

        ws.on('error', console.error);
    });

    wss.on('error', function(error) {
        console.log(error);
    });
}

function init(server) {
    return new WebSocket.Server({ server });
}

function heartbeat() {
  this.isAlive = true;
}

const WS = {
    init: server => {
        wss = init(server);
        setupListeners(wss);
        return wss;
    },
};

module.exports = {
    init: WS.init,
    wss,
};
