const mongoose = require('mongoose');
const cn = require('../const.js');

const PlayerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [
            function() {
                return this.role !== cn.ROLE.MONSTER;
            },
            'player has to have gender defined',
        ],
    },
    gender: {
        type: String,
        required: [
            function() {
                return this.role !== cn.ROLE.MONSTER;
            },
            'player has to have gender defined',
        ],
    },
    role: {
        type: String,
        required: true,
        default: cn.ROLE.PLAYER,
        enum: [
            cn.ROLE.GM,
            cn.ROLE.PLAYER,
            cn.ROLE.MONSTER,
        ],
    },
    level: {
        type: Number,
        required: true,
        default: 1,
    },
    bonuses: {
        type: Number,
        required: true,
        default: 0,
    },
    modifiers: {
        type: Number,
        required: true,
        default: 0,
    },
});

const FightSchema = new mongoose.Schema({
    status: {
        type: String,
        default: cn.FIGHT_STATUS.INACTIVE,
        enum: [
            cn.FIGHT_STATUS.ACTIVE,
            cn.FIGHT_STATUS.INACTIVE,
        ],
    },
    fighter: {
        type: PlayerSchema,
        required: false,
    },
    assistant: {
        type: PlayerSchema,
        required: false,
    },
    monster: {
        type: PlayerSchema,
        required: false,
    },
});

const RoomSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        dropDups: true,
    },
    password: {
        type: String,
        required: false,
    },
    players: {
        type: [ PlayerSchema ],
        default: [],
    },
    status: {
        type: String,
        default: cn.ROOM_STATUS.PENDING,
        enum: [
            cn.ROOM_STATUS.ACTIVE,
            cn.ROOM_STATUS.PENDING,
        ],
    },
    fight: {
        type: FightSchema,
        required: true,
    },
    currentPlayerIndex: {
        type: Number,
        default: 0,
    },
    createdAt: {
        type: Date,
        default: new Date(),
        required: true,
    },
});

// Player related methods
RoomSchema.methods.addPlayer = function(player) {
    this.players = [
        ...this.players,
        player,
    ];

    return this.save();
};

RoomSchema.methods.removePlayer = function(player) {
    this.players = this.players.filter(
        p => p._id.toString() !== player._id.toString()
    );

    return this.save();
};

RoomSchema.methods.updateCurrentPlayer = function(field, value) {
    const { players, currentPlayerIndex } = this;
    let updatedPlayer = players[currentPlayerIndex];

    console.log(value);
    const value_ = parseInt(value, 10);

    const players_ = players.map((player, index) => {
        if (
            index === currentPlayerIndex &&
            (field === 'level' ? player.level + value_ > 0 : true)
        ) {
            player[ field ] = player[ field ] + value_;
            updatedPlayer = player;
            return player;
        } else {
            return player;
        }
    });

    this.players = players_;

    return this.save().then(() => updatedPlayer);
};

// Status
RoomSchema.methods.updateStatus = function(status) {
    this.status = status;

    this.players = this.players.map(p => {
        p.level = 1;
        p.bonuses = 0;
        return p;
    });

    return this.save();
};

RoomSchema.methods.updateFight = function({ field, subField, value }) {
    if (field === 'status') {
        this.fight.status = value;
        if (value === cn.FIGHT_STATUS.INACTIVE) {
            this.fight.fighter = null;
            this.fight.monster = null;
        } else {
            this.fight.fighter = this.players[this.currentPlayerIndex];
            this.fight.monster = {
                role: cn.ROLE.MONSTER,
                level: 1,
                modifiers: 0,
            };
        }
    } else {
        const value_ = parseInt(value, 10);
        if (subField !== 'level' || this.fight[field][subField] + value_ > 0) {
            this.fight[field][subField] += value_;
        }
    }

    return this.save();
};

RoomSchema.methods.nextTurn = function() {
    this.currentPlayerIndex = this.currentPlayerIndex + 1;
    if (this.currentPlayerIndex >= this.players.length) {
        this.currentPlayerIndex = 0;
    }

    return this.save();
};

module.exports = mongoose.model('Room', RoomSchema);
