import React from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';

import Link from './Link';

import { COLORS } from '../constants';

export default ({ items }) => (
    <View style={ styles.nav }>
        {
            items && items.map(({ to, labelCode, icon }, index) => (
                <Link
                    key={ index }
                    icon={ icon }
                    to={ to }
                    labelCode={ labelCode }
                />
            ))
        }
    </View>
);

const styles = StyleSheet.create({
    nav: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: COLORS.darkerBrownFg,
    },
});
