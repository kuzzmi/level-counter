import { connect } from 'react-redux';
import Renderer from './renderer';
import { actions } from 'client/reducer/rooms/current';

const mapStateToProps = ({ rooms: { current } }) => {
    const { data, loading, error } = current;

    if (data) {
        const { players, currentPlayerIndex, status } = data;

        if (players.length > 0) {
            const players_ = players.map((player, index) => ({
                ...player,
                isCurrentPlayer: status === 'active' && currentPlayerIndex === index,
            }));

            return {
                ...current,
                data: {
                    ...data,
                    players: players_,
                },
                loading,
                error,
            };
        }
    }

    return current;
};

const mapDispatchToProps = actions;

export default connect(mapStateToProps, mapDispatchToProps)(Renderer);
