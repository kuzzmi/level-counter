const API_URL = 'http://192.168.0.122:3311';

const makeRequest = ({ method = 'GET', endpoint, body }) =>
    fetch(`${API_URL}/${ endpoint }`, {
        method,
        body: body ? JSON.stringify(body) : undefined,
        headers: {
            'content-type': 'application/json',
        },
    })
        .then(body => body.json())
        .then(json => {
            if (json.error) {
                return json;
            } else {
                return json;
            }
        });

const API = {
    rooms: {
        search: ({ query }) => {
            return makeRequest({
                endpoint: `rooms/?q=${ query }`,
            });
        },
        create: ({ name, password, profile }) => {
            const room = {
                name,
                password,
                players: [
                    profile,
                ],
            };

            return makeRequest({
                method: 'POST',
                endpoint: 'rooms',
                body: room,
            });
        },
    },
};

export default API;
