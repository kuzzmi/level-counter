import React from 'react';
import {
    Modal,
    View,
    StyleSheet,
} from 'react-native';

import Header from 'client/ui-kit/header';
import Button from 'client/ui-kit/button';
import { COLORS } from 'client/ui-kit/constants';

import I18n from 'client/i18n';

const noop = () => { /* noop */ };

export default ({
    visible = false,
    titleCode,
    close,
    onRequestClose,
    children,
}) => (
    <Modal
        onRequestClose={ onRequestClose || noop }
        animationType="slide"
        transparent={ false }
        visible={ visible }>
        <View style={ styles.page }>
            <View style={ styles.header }>
                <Button
                    onPress={ close }
                    icon={{
                        name: 'chevron-left',
                        size: 30,
                        color: COLORS.mustarderBg,
                    }}
                    style={ styles.button }
                />
                <Header>
                    { I18n.t(titleCode) }
                </Header>
            </View>
            <View style={ styles.body }>
                { children }
            </View>
        </View>
    </Modal>
);

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: COLORS.yellowBg,
        padding: 25,
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    backButton: {
        padding: 0,
        marginRight: 10,
    },

    body: {
        flex: 1,
    },
});
