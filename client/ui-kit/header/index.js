import React from 'react';

import {
    Text,
    StyleSheet,
} from 'react-native';

import { COLORS } from '../constants';

import I18n from '../../i18n';

export default ({
    text,
    textCode,
    level = 1,
    children,
}) => (
    <Text style={[
        styles.header,
        styles[`header-${level}`],
    ]}>
        { children || text || I18n.t(textCode) }
    </Text>
);

const styles = StyleSheet.create({
    header: {
        fontFamily: 'underdog',
    },

    'header-1': {
        fontSize: 28,
        color: COLORS.mustarderBg,
    },

    'header-2': {
        fontSize: 24,
        color: COLORS.mustarderBg,
    },

    'header-3': {
        fontSize: 20,
        color: COLORS.mustardBg,
    },

    'header-4': {
        fontSize: 16,
        color: COLORS.mustardBg,
        opacity: .8,
    },

    'header-5': {
        fontSize: 14,
        color: COLORS.mustardBg,
        opacity: .7,
    },
});
