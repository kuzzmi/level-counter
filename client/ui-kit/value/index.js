import React from 'react';
import {
    Text,
    View,
    StyleSheet,
} from 'react-native';

import I18n from '../../i18n';

import { COLORS } from '../constants';

export default ({
    horizontal,
    labelCode,
    value,
}) => (
    <View style={[
        styles.valueLabelContainer,
        horizontal ? styles.horizontalContainer : styles.verticalContainer,
    ]}>
        <Text style={[
            styles.label,
            styles.valueLabel,
        ]}>
            { value || 0 }
        </Text>
        <Text style={[
            styles.label,
            styles.valueLabelDesc,
        ]}>
            { I18n.t(labelCode) }
        </Text>
    </View>
);

const styles = StyleSheet.create({
    label: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
        fontSize: 14,
    },

    valueLabel: {
        fontSize: 28,
    },

    valueLabelDesc: {
        fontSize: 12,
    },

    valueLabelContainer: {
        alignItems: 'center',
        paddingHorizontal: 20,
    },

    verticalContainer: {
        paddingVertical: 10,
    },
});
