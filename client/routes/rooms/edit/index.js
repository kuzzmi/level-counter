import { connect } from 'react-redux';
import Renderer from './renderer';
import { actions } from 'client/reducer/rooms/current';

const mapStateToProps = ({ rooms: { current } }) => current;

const mapDispatchToProps = actions;

export default connect(mapStateToProps, mapDispatchToProps)(Renderer);
