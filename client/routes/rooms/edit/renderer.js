import React from 'react';
import {
    ActivityIndicator,
    View,
    StyleSheet,
} from 'react-native';

import { Redirect } from 'react-router-native';

import Header from 'client/ui-kit/header';
import Button from 'client/ui-kit/button';
import Modal from 'client/ui-kit/modal';

import PlayerEdit from 'client/modules/playerEdit';
import PlayersList from 'client/modules/playersList';

import I18n from 'client/i18n';

import { COLORS } from 'client/ui-kit/constants';

const PlayersListSuffix = removePlayer => ({ player }) => (
    <Button
        onPress={ () => removePlayer(player) }
        icon={{
            name: 'trash-2',
            size: 24,
            color: COLORS.darkBrownFg,
        }}
        style={ styles.button }
    />
);

export default ({
    data,
    loading,

    addingNewPlayer,
    addNewPlayer,
    removePlayer,
    cancelNewPlayer,
    saveNewPlayer,
    newPlayerName,
    newPlayerGender,
    updateNewPlayerName,
    updateNewPlayerGender,
    startGame,
}) => (
    loading ?
        <ActivityIndicator /> :
        data.status === 'active' ?
            <Redirect to="/rooms/play" /> :
            <View style={ styles.page }>
                <View style={ styles.header }>
                    <View>
                        <Header level={ 4 }>
                            { I18n.t('game.title') }
                        </Header>
                        <Header>
                            { data.name }
                        </Header>
                    </View>
                    <Button
                        onPress={ addNewPlayer }
                        icon={{
                            name: 'user-plus',
                            size: 24,
                            color: COLORS.mustarderBg,
                        }}
                        style={ styles.button }
                    />
                </View>
                <PlayersList
                    players={ data.players }
                    renderSuffix={ PlayersListSuffix(removePlayer) }
                />
                <View>
                    <Button
                        titleCode="game.startGame"
                        type="primary"
                        onPress={ startGame }
                    />
                    <Modal
                        titleCode="game.newPlayer"
                        visible={ addingNewPlayer }
                        close={ cancelNewPlayer }>
                        <View style={{ flex: 1 }}>
                            <PlayerEdit
                                name={ newPlayerName }
                                gender={ newPlayerGender }
                                updateName={ updateNewPlayerName }
                                updateGender={ updateNewPlayerGender }
                            />
                            <Button
                                titleCode="game.save"
                                type="primary"
                                onPress={ saveNewPlayer }
                            />
                        </View>
                    </Modal>
                </View>
            </View>
);

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },

    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    button: {
        padding: 0,
    },

    label: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
    },

    labelActive: {
        color: COLORS.yellowBg,
    },

    labelActiveWarning: {
        fontSize: 28,
    },

    listItemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 4,
        paddingHorizontal: 25,
    },

    nameContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },

    playerName: {
        fontSize: 16,
        marginRight: 10,
    },

    controls: {
        marginTop: 10,
        margin: -25,
        paddingVertical: 10,
        paddingHorizontal: 25,

        flexDirection: 'row',
        alignItems: 'center',
    },

    controlButton: {
        width: 80,
        height: 80,
        borderRadius: 999,
        justifyContent: 'center',
        alignItems: 'center',
    },

    controlValue: {
        paddingTop: 10,
        fontSize: 28,
    },

    controlValueLabel: {
        paddingBottom: 10,
        fontSize: 12,
    },

    listItemStats: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    statLabel: {
        fontSize: 22,
    },

    statTotalLabel: {
        fontSize: 28,
    },

    separator: {
        opacity: .8,
        paddingHorizontal: 10,
        fontFamily: 'underdog',
        color: COLORS.darkBrownFg,
        fontSize: 14,
    },
});
