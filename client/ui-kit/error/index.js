import React from 'react';
import {
    View,
    Text,
    StyleSheet,
} from 'react-native';

import I18n from '../../i18n';

export default ({ error, namespace }) =>
    error ?
        <View style={ styles.errorContainer }>
            <Text style={ styles.errorText }>
                { I18n.t(
                    `errors.${namespace}.${error.code}`,
                    {
                        defaults: [{
                            message: JSON.stringify(error),
                        }],
                    }
                ) }
            </Text>
        </View> : null;

const styles = StyleSheet.create({
    errorContainer: {
        marginVertical: 10,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: '#d14836',
        borderRadius: 6,
    },

    errorText: {
        fontFamily: 'underdog',
        fontSize: 14,
        color: 'white',
        textAlign: 'center',
    },
});
