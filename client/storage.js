import { AsyncStorage } from 'react-native';

const PREFIX = '@LevelCounterAdvanced';

const makeName = name => `${PREFIX}:${name}`;

export const get = name =>
    AsyncStorage.getItem(makeName(name))
        .then(data => {
            try {
                const json = data ? JSON.parse(data) : data;
                return json;
            } catch (e) {
                return data;
            }
        });

export const set = (name, data) =>
    AsyncStorage.setItem(makeName(name), JSON.stringify(data));

export default {
    get,
    set,
};
