export default url => {
    let ws;

    function initWs(dispatch) {
        const ws_ = new WebSocket(url);

        ws_.addEventListener('open', function() {
            ws_.addEventListener('message', ({ data }) => {
                try {
                    const { event, payload } = JSON.parse(data);
                    dispatch({
                        type: event,
                        payload,
                    });
                } catch (e) {
                }
            });
        });

        ws_.addEventListener('close', initWs);

        return ws_;
    }

    return ({ dispatch }) => {
        if (ws === undefined || ws.readyState === WebSocket.CLOSED) {
            ws = initWs(dispatch);
        }

        return next => action => {
            const state = next(action);

            if (!action.type) {
                return state;
            }

            const [ prefix, suffix ] = action.type.split('/');

            if (!suffix || prefix !== '@WS') {
                return state;
            }

            const payload = JSON.stringify({
                event: suffix.toLowerCase(),
                payload: action.payload,
            });

            ws.send(payload);
        };
    };
};
