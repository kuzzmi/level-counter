import React from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
} from 'react-native';

import List from '../../ui-kit/list';
import Icon from '../../ui-kit/icon';
import { COLORS } from '../../ui-kit/constants';

const PlayersListItem = ({ item, index, onItemPress, renderSuffix: Suffix }) => (
    <TouchableOpacity
        onPress={
            onItemPress ?
                () => onItemPress(index) :
                () => { /* do nothing */ }
        }>
        <View style={[
            styles.playerListItem,
            item.isCurrentPlayer ? styles.listItemActiveContainer : null,
        ]}>
            <View style={ styles.nameContainer }>
                <Icon
                    family="MaterialCommunityIcons"
                    name={ `gender-${ item.gender }` }
                    size={ 20 }
                    color={
                        item.isCurrentPlayer ? COLORS.yellowBg : COLORS.darkBrownFg
                    }
                />
                <Text style={[
                    styles.label,
                    styles.playerListItemName,
                    item.isCurrentPlayer ? styles.labelActive : null,
                ]}>
                    { item.name }
                </Text>
                {
                    item.role === 'gm' &&
                    <Icon
                        family="Foundation"
                        name="crown"
                        color={
                            item.isCurrentPlayer ? COLORS.yellowBg : COLORS.darkBrownFg
                        }
                        size={ 20 }
                    />
                }
            </View>
            <View style={ styles.suffixContainer }>
                { Suffix ? <Suffix player={ item } /> : null }
            </View>
        </View>
    </TouchableOpacity>
);

export default ({
    players,
    emptyLabelCode,
    renderSuffix,
    onItemPress,
}) => (
    <List
        data={ players }
        emptyLabelCode={ emptyLabelCode }
        renderItem={ PlayersListItem }
        renderSuffix={ renderSuffix }
        onItemPress={ onItemPress }
    />
);

const styles = StyleSheet.create({
    playerListItem: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 4,
        paddingHorizontal: 25,
        minHeight: 50,
    },

    nameContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },

    playerListItemName: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
        fontSize: 16,
        marginHorizontal: 10,
    },

    suffixContainer: {
        flexDirection: 'row',
    },

    label: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
    },

    listItemActiveContainer: {
        backgroundColor: COLORS.mustardBg,
    },

    labelActive: {
        color: COLORS.yellowBg,
    },
});
