import React from 'react';
import {
    View,
    Text,
    StyleSheet,
} from 'react-native';

import { Redirect } from 'react-router-native';

import Header from 'client/ui-kit/header';
import Button from 'client/ui-kit/button';
import Value from 'client/ui-kit/value';
import Counter from 'client/ui-kit/counter';

import I18n from 'client/i18n';

import { COLORS } from 'client/ui-kit/constants';

export default ({
    fighter,
    monster,
    status,

    stopFight,
    incrementFighterModifiers,
    decrementFighterModifiers,
    incrementMonsterLevel,
    decrementMonsterLevel,
    incrementMonsterModifiers,
    decrementMonsterModifiers,
}) => {
    if (status === 'inactive') {
        return <Redirect to="/rooms/play" />;
    }

    const totalFighter = fighter.level + fighter.bonuses + fighter.modifiers;
    const totalMonster = monster.level + monster.modifiers;

    return (
        <View style={ styles.page }>
            <View style={ styles.header }>
                <View>
                    <Header level={ 4 }>
                        { I18n.t('battle.title') }
                    </Header>
                    <Header>
                        { fighter.name } { I18n.t('battle.versus') } { I18n.t('battle.monster') }
                    </Header>
                </View>
                <Button
                    onPress={ stopFight }
                    icon={{
                        family: 'MaterialCommunityIcons',
                        name: 'undo',
                        size: 24,
                        color: COLORS.mustarderBg,
                    }}
                    style={ styles.button }
                />
            </View>
            <View style={ styles.content }>
                <View style={ styles.block }>
                    <View style={ styles.staticValuesContainer }>
                        <Value
                            labelCode="game.level"
                            value={ fighter.level }
                            horizontal
                        />
                        <Value
                            labelCode="game.bonus"
                            value={ fighter.bonuses }
                            horizontal
                        />
                    </View>
                    <View style={ styles.controls }>
                        <Counter
                            labelCode="battle.modifiers"
                            value={ fighter.modifiers }
                            horizontal
                            onPressUp={ incrementFighterModifiers }
                            onPressDown={ decrementFighterModifiers }
                        />
                    </View>
                </View>
                <View style={ styles.scoreContainer }>
                    <View style={ styles.line } />
                    <View style={ styles.circles }>
                        <View style={[
                            styles.circle,
                            totalFighter > totalMonster ? styles.circleWinner : null,
                        ]}>
                            <Text style={[
                                styles.scoreLabel,
                                totalFighter > totalMonster ? styles.scoreLabelWinner : null,
                            ]}>
                                { totalFighter }
                            </Text>
                        </View>
                        <View style={[
                            styles.circle,
                            totalFighter <= totalMonster ? styles.circleWinner : null,
                        ]}>
                            <Text style={[
                                styles.scoreLabel,
                                totalFighter <= totalMonster ? styles.scoreLabelWinner : null,
                            ]}>
                                { totalMonster }
                            </Text>
                        </View>
                    </View>
                </View>
                <View style={ styles.block }>
                    <View style={ styles.controls }>
                        <Counter
                            labelCode="game.level"
                            value={ monster.level }
                            horizontal
                            onPressUp={ incrementMonsterLevel }
                            onPressDown={ decrementMonsterLevel }
                        />
                        <Counter
                            labelCode="battle.modifiers"
                            value={ monster.modifiers }
                            horizontal
                            onPressUp={ incrementMonsterModifiers }
                            onPressDown={ decrementMonsterModifiers }
                        />
                    </View>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },

    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    button: {
        padding: 0,
    },

    block: {
        flex: 1,
    },

    content: {
        flex: 1,
    },

    label: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
        fontSize: 14,
    },

    circles: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },

    circle: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 70,
        height: 70,
        borderRadius: 999,
        backgroundColor: COLORS.darkerBrownFg,
    },

    circleWinner: {
        backgroundColor: COLORS.mustarderBg,
    },

    scoreLabel: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
        fontSize: 32,
    },

    scoreLabelWinner: {
        color: COLORS.yellowBg,
    },

    controls: {
        flex: 1,
    },

    staticValuesContainer: {
        paddingVertical: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
});
