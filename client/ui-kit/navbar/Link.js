import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Animated,
} from 'react-native';

import {
    Route,
    Link,
} from 'react-router-native';

import { COLORS } from '../constants';

import Icon from '../icon';

import I18n from '../../i18n.js';

export default class extends Component {
    render() {
        const {
            labelCode,
            to,
            exact,
            icon,
        } = this.props;

        return (
            <Route path={ to } exact={ exact } children={({ match }) => (
                <Link
                    style={ styles.link }
                    underlayColor={ COLORS.darkerBrownFg }
                    to={ to }>
                    <View>
                        <AnimatedLink
                            label={ I18n.t(`nav.${ labelCode }`) }
                            icon={ icon }
                            isActive={ match }
                        />
                    </View>
                </Link>
            )} />
        );
    }
}

class AnimatedLink extends React.Component {
    state = {
        activeAnim: new Animated.Value(0),
    };

    componentDidMount() {
        const { isActive } = this.props;
        Animated.timing(
            this.state.activeAnim,
            {
                toValue: isActive ? 1 : 0,
                duration: 200,
            }
        ).start();
    }

    componentWillReceiveProps({ isActive }) {
        Animated.timing(
            this.state.activeAnim,
            {
                toValue: isActive ? 1 : 0,
                duration: 200,
            }
        ).start();
    }

    render() {
        const { icon, label } = this.props;

        return (
            <Animated.View style={[
                styles.container,
                {
                    opacity: this.state.activeAnim.interpolate({
                        inputRange: [0, 1],
                        outputRange: [.3, 1],
                    }),
                },
            ]}>
                <Icon { ...icon } color={ COLORS.mustardBg } size={ 24 } />
                <Text style={ styles.navItemText }>
                    { label }
                </Text>
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    link: {
        flex: 1,
        alignItems: 'center',
        padding: 10,
    },
    container: {
        alignItems: 'center',
    },
    navItemText: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
        fontSize: 12,
        marginTop: 3,
    },
});
