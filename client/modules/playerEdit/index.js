import React from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';

import Input from 'client/ui-kit/input';
import Header from 'client/ui-kit/header';
import Button from 'client/ui-kit/button';
import { COLORS } from 'client/ui-kit/constants';

import I18n from 'client/i18n';

export default ({
    name,
    gender,
    updateName,
    updateGender,
}) => (
    <View style={ styles.form }>
        <View>
            <Input
                placeholderCode="profile.name"
                value={ name }
                onChange={ updateName }
                autoFocus={ !name }
            />
        </View>
        <View style={ styles.genderSelectorContainer }>
            <Header level={ 3 }>
                { I18n.t('profile.sex') }
            </Header>
        </View>
        <View style={ styles.genderSelector }>
            <View style={ styles.genderButtonContainer }>
                <Button
                    style={[
                        styles.genderButton,
                        {
                            backgroundColor: gender === 'female' ? COLORS.mustardBg : COLORS.darkerBrownFg,
                        },
                    ]}
                    type="primary"
                    icon={{
                        family: 'MaterialCommunityIcons', name: 'gender-female',
                        size: 40,
                        color: gender === 'female' ? COLORS.yellowBg : COLORS.mustardBg,
                    }}
                    onPress={ () => updateGender('female') }
                />
                <Header level={ 4 }>
                    { I18n.t('profile.female') }
                </Header>
            </View>
            <View style={ styles.genderButtonContainer }>
                <Button
                    style={[
                        styles.genderButton,
                        {
                            backgroundColor: gender === 'male' ? COLORS.mustardBg : COLORS.darkerBrownFg,
                        },
                    ]}
                    type="primary"
                    icon={{
                        family: 'MaterialCommunityIcons',
                        name: 'gender-male',
                        size: 40,
                        color: gender === 'male' ? COLORS.yellowBg : COLORS.mustardBg,
                    }}
                    onPress={ () => updateGender('male') }
                />
                <Header level={ 4 }>
                    { I18n.t('profile.male') }
                </Header>
            </View>
        </View>
    </View>
);

const styles = StyleSheet.create({
    form: {
        flex: 1,
    },

    genderSelectorContainer: {
        alignItems: 'center',
        marginVertical: 20,
    },

    genderSelector: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },

    genderButton: {
        height: 70,
        width: 70,
        borderRadius: 999,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
    },

    genderButtonContainer: {
        alignItems: 'center',
    },
});
