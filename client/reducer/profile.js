import Storage from 'client/storage';

export const initialState = {
    name: '',
    gender: '',
};

export const types = {
    UPDATE_NAME: 'PROFILE_UPDATE_NAME',
    UPDATE_GENDER: 'PROFILE_UPDATE_GENDER',
    LOAD: 'PROFILE_LOAD',
};

export const actions = {
    updateName: value => ({
        type: types.UPDATE_NAME,
        payload: {
            value,
        },
    }),

    updateGender: value => ({
        type: types.UPDATE_GENDER,
        payload: {
            value,
        },
    }),

    request: () => {
        return dispatch => {
            return Storage.get('profile')
                .then(data => {
                    const profile = data || {
                        name: '',
                        gender: 'female',
                    };

                    dispatch({
                        type: types.LOAD,
                        payload: profile,
                    });
                });
        };
    },

    save: (history, state) => (dispatch, getState) => {
        const { redirect } = state || {};
        return Storage
            .set('profile', getState().profile)
            .then(() => history.push(redirect || '/rooms/'));
    },
};

const handlers = {
    [types.LOAD]: (state, { name, gender }) => ({
        name,
        gender,
    }),

    [types.UPDATE_NAME]: (state, { value }) => ({
        ...state,
        name: value,
    }),

    [types.UPDATE_GENDER]: (state, { value }) => ({
        ...state,
        gender: value === 'female' ? value : 'male',
    }),
};

export default (state = initialState, { type, payload }) => {
    const handler = handlers[type];

    const state_ = handler ? handler(state, payload) : state;

    return state_;
};
