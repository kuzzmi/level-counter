import React from 'react';
import { StyleSheet } from 'react-native';

import { withRouter } from 'react-router-native';
import Button from '../button';
import { COLORS } from '../constants';

const BackButton = ({ history }) => (
    <Button
        onPress={ history.goBack }
        icon={{
            name: 'chevron-left',
            size: 30,
            color: COLORS.mustarderBg,
        }}
        style={ styles.button }
    />
);

const styles = StyleSheet.create({
    button: {
        padding: 0,
        marginRight: 10,
    },
});

export default withRouter(BackButton);
