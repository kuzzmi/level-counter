import DefaultLayout from '../../layouts/default';

import Profile from './profile';

export default [{
    path: '/profile',
    exact: true,
    component: Profile,
    layout: DefaultLayout,
    navbar: {
        labelCode: 'profile',
        icon: {
            family: 'FontAwesome',
            name: 'user',
        },
    },
}];
