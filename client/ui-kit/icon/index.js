import React from 'react';

import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Foundation from 'react-native-vector-icons/Foundation';

const Icon = ({ family, ...rest }) => {
    switch (family) {
        case 'MaterialCommunityIcons':
            return <MaterialCommunityIcons { ...rest } />;
        case 'Foundation':
            return <Foundation { ...rest } />;
        case 'FontAwesome':
            return <FontAwesome { ...rest } />;
        case 'Feather':
            return <Feather { ...rest } />;
        default:
            return <Feather { ...rest } />;
    }
};

export default ({
    family,
    name,
    size = 16,
    color = '#3c3c3c',
}) => (
    <Icon family={ family } name={ name } size={ size } color={ color } />
);
