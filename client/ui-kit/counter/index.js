import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Modal,
} from 'react-native';

import Button from '../button';
import Value from '../value';
import Input from '../input';
import Header from '../header';

import { COLORS } from '../constants';

class Counter extends Component {
    state = {
        modalVisible: false,
        minus: false,
        value: '',
    };

    constructor(props) {
        super(props);

        this.openModal = () => {
            this.setState(state => ({
                ...state,
                modalVisible: true,
            }));
        };

        this.hideModal = () => {
            this.setState(state => ({
                ...state,
                value: '',
                modalVisible: false,
            }));
        };

        this.updateValue = value => {
            console.log(value);
            this.setState(state => ({
                ...state,
                value,
            }));
        };

        this.setPlus = () => {
            this.setState(state => ({
                ...state,
                minus: false,
            }));
        };

        this.setMinus = () => {
            this.setState(state => ({
                ...state,
                minus: true,
            }));
        };

        this.save = () => {
            const { minus, value } = this.state;

            if (minus && value) {
                this.props.onPressDown(value);
            } else {
                this.props.onPressUp(value);
            }

            this.hideModal();
        };
    }

    render() {
        const {
            labelCode,
            value,
            onPressUp,
            onPressDown,
            horizontal,
        } = this.props;

        return (
            <View
                style={[
                    styles.control,
                    horizontal ? styles.horizontal : styles.vertical,
                ]}>
                <Button
                    type="primary"
                    icon={{
                        name: 'arrow-up',
                        color: COLORS.mustardBg,
                    }}
                    style={ styles.controlButton }
                    onPress={ () => onPressUp(1) }
                />
                <TouchableOpacity
                    onPress={ this.openModal }>
                    <Value
                        labelCode={ labelCode }
                        value={ value }
                    />
                </TouchableOpacity>
                <Button
                    type="primary"
                    icon={{
                        name: 'arrow-down',
                        color: COLORS.mustardBg,
                    }}
                    style={ styles.controlButton }
                    onPress={ () => onPressDown(1) }
                />
                <Modal
                    animationType="fade"
                    transparent={ true }
                    visible={ this.state.modalVisible }
                    onRequestClose={ this.hideModal }>
                    <View style={ styles.modalContainer }>
                        <View style={ styles.modalBody }>
                            <View style={ styles.modalHeader }>
                                <Header
                                    level={ 2 }
                                    textCode="game.multiLevel"
                                />
                                <Button
                                    type="default"
                                    style={{
                                        marginTop: -14,
                                        marginRight: -10,
                                    }}
                                    icon={{
                                        family: 'MaterialCommunityIcons',
                                        name: 'window-close',
                                        size: 26,
                                        color: COLORS.mustardBg,
                                    }}
                                    onPress={ this.hideModal }
                                />
                            </View>
                            <View style={ styles.inputContainer }>
                                <Button
                                    type="primary"
                                    icon={{
                                        family: 'MaterialCommunityIcons',
                                        name: 'plus',
                                        size: 26,
                                        color: this.state.minus ? COLORS.mustardBg : COLORS.yellowBg,
                                    }}
                                    style={{
                                        backgroundColor: this.state.minus ? COLORS.yellowBg : COLORS.mustardBg,
                                    }}
                                    onPress={ this.setPlus }
                                />
                                <View style={{ width: 10 }} />
                                <Button
                                    type="primary"
                                    icon={{
                                        family: 'MaterialCommunityIcons',
                                        name: 'minus',
                                        size: 26,
                                        color: this.state.minus ? COLORS.yellowBg : COLORS.mustardBg,
                                    }}
                                    style={{
                                        backgroundColor: this.state.minus ? COLORS.mustardBg : COLORS.yellowBg,
                                    }}
                                    onPress={ this.setMinus }
                                />
                                <View style={{ width: 10 }} />
                                <Input
                                    autoFocus={ true }
                                    style={{ flex: 1 }}
                                    value={ this.state.value }
                                    type="numeric"
                                    placeholderCode="game.multiLevel"
                                    onChange={ this.updateValue }
                                />
                            </View>
                            <Button
                                type="primary"
                                titleCode="game.multiSave"
                                onPress={ this.save }
                            />
                            <Button
                                type="default"
                                titleCode="game.multiCancel"
                                onPress={ this.hideModal }
                            />
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    label: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
        fontSize: 14,
    },

    control: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    horizontal: {
        flexDirection: 'row',
    },

    vertical: {
    },

    modalContainer: {
        backgroundColor: 'rgba(0,0,0,.4)',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    modalHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
    },

    modalBody: {
        width: '90%',
        padding: 20,
        backgroundColor: COLORS.yellowBg,
        borderRadius: 6,
        elevation: 2,
    },

    inputContainer: {
        flexDirection: 'row',
        marginBottom: 10,
    },

    controlButton: {
        padding: 10,
        borderRadius: 999,
        alignItems: 'center',
    },
});

export default Counter;
