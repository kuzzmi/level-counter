import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import profile, { initialState as profileState } from './profile';
import rooms, { initialState as roomsState } from './rooms';

export const initialState = {
    profile: profileState,
    rooms: roomsState,
};

export default combineReducers({
    profile,
    rooms,
    router: routerReducer,
});
