import React from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';

import Header from '../../../ui-kit/header';
import Button from '../../../ui-kit/button';
import I18n from '../../../i18n';

import PlayerEdit from 'client/modules/playerEdit';

export default ({
    name,
    gender,

    updateName,
    updateGender,
    save,

    history,
    location: { state },
}) => (
    <View style={ styles.page }>
        <View style={ styles.header }>
            <Header>
                { I18n.t('profile.title') }
            </Header>
            <Header level={ 5 }>
                { I18n.t('profile.description') }
            </Header>
        </View>
        <PlayerEdit
            name={ name }
            gender={ gender }
            updateName={ updateName }
            updateGender={ updateGender }
        />
        <Button
            type="primary"
            titleCode="profile.save"
            onPress={ () => save(history, state) }
        />
    </View>
);

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },

    header: {
        marginBottom: 10,
    },
});

