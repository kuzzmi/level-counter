import Storage from 'client/storage';
import { combineReducers } from 'redux';
import { push } from 'react-router-redux';

import create, { initialState as createState } from './create.js';
import search, { initialState as searchState } from './search.js';
import current, {
    initialState as currentState,
    actions as currentActions,
} from './current.js';
import history, {
    initialState as historyState,
    actions as historyActions,
} from './history.js';

export const initialState = {
    history: historyState,
    search: searchState,
    create: createState,
    current: currentState,
};

export const actions = {
    join: room => (dispatch, getState) => {
        dispatch({
            type: '@WS/JOIN',
            payload: {
                roomId: room._id,
            },
        });

        dispatch(currentActions.load());

        dispatch(push('/rooms/edit'));

        const rooms = getState().rooms.history.data || [];

        const hasCurrentRoom = rooms.filter(r => r._id === room._id).length > 0;

        if (!hasCurrentRoom) {
            rooms.push(room);

            dispatch(historyActions.load(rooms));

            return Storage.set('history', rooms);
        }
    },
};

export default combineReducers({
    history,
    create,
    current,
    search,
});
