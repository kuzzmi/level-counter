import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import {
    StyleSheet,
    StatusBar,
    View,
} from 'react-native';

import {
    BackButton,
    Redirect,
    Switch,
} from 'react-router-native';

import I18n from './i18n.js';
import createStore, { history } from './createStore';

import {
    AppLoading,
    Font,
} from 'expo';

import Navbar from './ui-kit/navbar';

import RoomsRoutes from './routes/rooms';
import ProfileRoutes from './routes/profile';

const routes = [
    ...RoomsRoutes,
    ...ProfileRoutes,
];

const makeNavItems = routes =>
    routes
        .filter(route => !!route.navbar)
        .map(route => ({
            to: route.path,
            ...route.navbar,
        }));

const NavItems = makeNavItems(routes);

const renderRoutes = routes =>
    routes.map(({ layout: Layout, ...rest }, index) => (
        <Layout
            { ...rest }
            key={ index }
        />
    ));

const store = createStore();

export default class App extends React.Component {
    state = {
        loaded: false,
    };

    componentWillMount() {
        this._loadAssetsAsync();
    }

    _loadAssetsAsync = async() => {
        await Font.loadAsync({
            underdog: require('./assets/fonts/Underdog-Regular.ttf'),
        });
        await I18n.initAsync();
        this.setState({
            loaded: true,
        });
    };

    render() {
        if (!this.state.loaded) {
            return <AppLoading />;
        }

        return (
            <Provider store={ store }>
                <ConnectedRouter history={ history }>
                    <View style={ styles.app }>
                        <BackButton />
                        <Switch>
                            { renderRoutes(routes) }
                            <Redirect to="/rooms/history" />
                        </Switch>
                        <Navbar items={ NavItems } />
                    </View>
                </ConnectedRouter>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    app: {
        flex: 1,
    },
});
