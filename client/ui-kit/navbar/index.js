import Navbar from './Navbar';
import Link from './Link';

Navbar.Link = Link;

export default Navbar;
