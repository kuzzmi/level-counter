import React from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';

import Header from '../../../ui-kit/header';
import Button from '../../../ui-kit/button';

import RoomsList from '../../../modules/roomsList';

import I18n from '../../../i18n';

export default ({
    data: rooms,
    join,
}) => (
    <View style={ styles.page }>
        <View style={ styles.header }>
            <Header>
                { I18n.t('rooms.list.title') }
            </Header>
            <Header level={ 5 }>
                { I18n.t('rooms.list.description') }
            </Header>
        </View>
        <RoomsList
            rooms={ rooms }
            emptyLabelCode="rooms.list.emptyList"
            notAskedLabelCode="rooms.list.emptyList"
            onPress={ join }
        />
        <View style={ styles.buttons }>
            <Button
                titleCode="rooms.list.create"
                linkTo="/rooms/create"
            />
            <View style={{ height: 5 }} />
            <Button
                titleCode="rooms.list.find"
                linkTo="/rooms/search"
            />
        </View>
    </View>
);

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
});
