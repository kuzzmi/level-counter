const app = require('./app.js');
const http = require('http');
const server = http.createServer(app);
const WS = require('./ws.js');

const PORT = 3311;

server.listen(PORT, function() {
    console.log('Listening on %d', server.address().port);
});

server.on('clientError', error => {
    console.log(error);
})

WS.init(server);
