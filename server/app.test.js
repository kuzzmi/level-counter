const request = require('supertest');
const app = require('./app.js');

const mongoose = require('mongoose');
const Room = require('./models/room.js');

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/level-counter', {
    useMongoClient: true,
});

describe('API', () => {
    let testRoomId;

    const testGM1 = {
        name: 'GM 1',
        role: 'gm',
        level: 1,
        power: 0,
    };

    const testGM2 = {
        name: 'GM 2',
        role: 'gm',
        level: 1,
        power: 0,
    };

    const testPlayer1 = {
        name: 'Player 1',
        role: 'player',
        level: 1,
        power: 0,
    };

    const testPlayer2 = {
        name: 'Player 2',
        role: 'player',
        level: 1,
        power: 0,
    };

    const testBattle1 = {
        players: [ testPlayer1 ],
        monsters: [ testPlayer2 ],
    };

    beforeAll(done => {
        Room.remove({}).then(() => done());

        const room = {
            name: 'Test room',
            players: [ testGM1, testPlayer1 ],
        };

        Room.create(room).then(({ _id }) => {
            testRoomId = _id;
            done();
        });
    });

    afterAll(done => {
        Room.remove({}).then(() => done());
    });

    it('should return a list of rooms with one test room', done => {
        request(app)
            .get('/rooms/')
            .expect(res => {
                expect(res.body.rooms.length).toBe(1);
            })
            .expect(200, done);
    });

    it('should pass a turn to the next player', () => {
        return request(app)
            .post(`/rooms/${ testRoomId }/next_player/`)
            .expect(res => {
                expect(res.body.status).toBe('OK');
            })
            .expect(200)
            .then(() => {
                return request(app)
                    .get('/rooms/')
                    .expect(res => {
                        expect(res.body.rooms[0].currentPlayerIndex).toBe(1);
                    })
                    .expect(200);
            })
            .then(() => {
                return request(app)
                    .post(`/rooms/${ testRoomId }/next_player/`)
                    .expect(res => {
                        expect(res.body.status).toBe('OK');
                    })
                    .expect(200);
            })
            .then(() => {
                return request(app)
                    .get('/rooms/')
                    .expect(res => {
                        expect(res.body.rooms[0].currentPlayerIndex).toBe(0);
                    })
                    .expect(200);
            })
    });

    it('should add a new player to the game', () => {
        return request(app)
            .post(`/rooms/${ testRoomId }/connect/`)
            .send(testPlayer2)
            .expect(res => {
                expect(res.body.status).toBe('OK');
            })
            .expect(200)
            .then(() => {
                return request(app)
                    .get('/rooms/')
                    .expect(res => {
                        expect(res.body.rooms[0].players.length).toBe(3);
                    })
                    .expect(200);
            });
    });

    it('should not add a new player to the game with existing name', () => {
        return request(app)
            .post(`/rooms/${ testRoomId }/connect/`)
            .send(testPlayer2)
            .expect(res => {
                expect(res.body.status).toBe('error');
                expect(res.body.message).toMatch('with this name');
            })
            .expect(200);
    });

    it('should not add another GM to the game', () => {
        return request(app)
            .post(`/rooms/${ testRoomId }/connect/`)
            .send(testGM2)
            .expect(res => {
                expect(res.body.status).toBe('error');
                expect(res.body.message).toMatch('one GM');
            })
            .expect(200);
    });

    it('should remove user from the room', () => {
        return request(app)
            .post(`/rooms/${ testRoomId }/disconnect/`)
            .send(testPlayer2)
            .expect(res => {
                expect(res.body.status).toBe('OK');
            })
            .then(() => {
                return request(app)
                    .get('/rooms/')
                    .expect(res => {
                        expect(res.body.rooms[0].players.length).toBe(2);
                    })
                    .expect(200);
            });
    });

    it('should allow updating player\'s level', () => {
        return request(app)
            .post(`/rooms/${ testRoomId }/update_player/`)
            .send({
                ...testPlayer1,
                level: 10,
            })
            .expect(res => {
                expect(res.body.status).toBe('OK');
            })
            .then(() => {
                return request(app)
                    .get('/rooms/')
                    .expect(res => {
                        expect(res.body.rooms[0].players.length).toBe(2);
                    })
                    .expect(200);
            });
    });

    it('should allow updating player\'s power', () => {
        return request(app)
            .post(`/rooms/${ testRoomId }/update_player/`)
            .send({
                ...testPlayer1,
                power: 10,
            })
            .expect(res => {
                expect(res.body.status).toBe('OK');
            })
            .then(() => {
                return request(app)
                    .get('/rooms/')
                    .expect(res => {
                        expect(res.body.rooms[0].players.length).toBe(2);
                    })
                    .expect(200);
            });
    });

    it('should not allow updating non-existing player\'s level', () => {
        return request(app)
            .post(`/rooms/${ testRoomId }/update_player/`)
            .send({
                ...testPlayer2,
                level: 10,
            })
            .expect(res => {
                expect(res.body.status).toBe('error');
                expect(res.body.message).toMatch('not found');
            })
            .then(() => {
                return request(app)
                    .get('/rooms/')
                    .expect(res => {
                        expect(res.body.rooms[0].players.length).toBe(2);
                    })
                    .expect(200);
            });
    });

    xit('should allow a battle to start', () => {
        return request(app)
            .post(`/rooms/${ testRoomId }/start_battle/`)
            .send(testBattle1)
            .expect(res => {
                expect(res.body.status).toBe('OK');
            })
            .then(() => {
                return request(app)
                    .get('/rooms/')
                    .expect(res => {
                        expect(res.body.rooms[0].battle).toBeDefined();
                    })
                    .expect(200);
            });
    });

});
