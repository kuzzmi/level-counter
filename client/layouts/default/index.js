import React from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';

import { Route } from 'react-router';
import { COLORS } from 'client/ui-kit/constants';

export default ({
    component: Component,
    ...rest
}) => (
    <Route { ...rest } render={ matchProps => (
        <View style={ styles.container }>
            <View style={ styles.page }>
                <Component { ...matchProps } />
            </View>
        </View>
    )} />
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.yellowBg,
        marginTop: 25,
        padding: 0,
    },

    page: {
        flex: 1,
        margin: 15,
        padding: 10,
    },
});
