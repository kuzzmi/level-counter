import React from 'react';
import {
    ActivityIndicator,
    TouchableOpacity,
    StyleSheet,
    View,
    Text,
    FlatList,
} from 'react-native';

import Icon from '../../ui-kit/icon';
import { COLORS } from '../../ui-kit/constants';

import I18n from '../../i18n';

const EmptyList = ({ notAsked, emptyCode, notAskedCode, loading }) => (
    <View style={ styles.emptyList }>
        {
            loading ?
                <ActivityIndicator color={ COLORS.mustardBg } size="large" /> :
                <Text style={ styles.label }>
                    { notAsked ? I18n.t(notAskedCode) : I18n.t(emptyCode) }
                </Text>
        }
    </View>
);

const RoomsListItem = ({ item, onPress }) => (
    <TouchableOpacity onPress={ () => onPress(item) }>
        <View style={ styles.roomListItem }>
            <View style={ styles.nameContainer }>
                <Text style={ styles.roomListItemRoomName }>{ item.name }</Text>
                <Text style={ styles.roomListItemGmName }>{ I18n.t('rooms.list.gm') }: { item.players[0].name }</Text>
            </View>
            <Icon name="chevron-right" size={ 30 } color={ COLORS.darkBrownFg } />
        </View>
    </TouchableOpacity>
);

export default ({
    rooms,
    loading,
    emptyLabelCode,
    notAskedLabelCode,
    onPress,
}) => (
    <View style={ styles.listContainer }>
        {
            (rooms === null || rooms.length === 0 || loading === true) ?
                <EmptyList
                    notAsked={ rooms === null }
                    emptyCode={ emptyLabelCode }
                    notAskedCode={ notAskedLabelCode }
                    loading={ loading }
                /> :
                <FlatList
                    data={ rooms }
                    renderItem={ props => <RoomsListItem { ...props } onPress={ onPress } /> }
                    ListEmptyComponent={ EmptyList }
                    keyExtractor={ (item, index) => index }
                />
        }
    </View>
);

const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        marginVertical: 10,
    },

    emptyList: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 20,
    },

    label: {
        fontFamily: 'underdog',
        fontSize: 18,
        marginVertical: 20,
        textAlign: 'center',
        color: COLORS.grey,
        opacity: .7,
    },

    nameContainer: {
        flex: 1,
        justifyContent: 'center',
    },

    roomListItem: {
        flex: 1,
        paddingVertical: 10,
        minHeight: 50,
        flexDirection: 'row',
        alignItems: 'center',
    },

    roomListItemRoomName: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
        fontSize: 18,
    },

    roomListItemGmName: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
        opacity: .7,
        fontSize: 14,
    },
});
