import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    FlatList,
} from 'react-native';

import { COLORS } from '../constants';

import I18n from '../../i18n';

const EmptyList = ({ code }) => (
    <View style={ styles.emptyList }>
        <Text style={ styles.label }>
            { I18n.t(code) }
        </Text>
    </View>
);

export default ({
    data,
    emptyLabelCode,
    renderItem,
    renderSuffix,
    onItemPress,
}) => (
    <View style={ styles.listContainer }>
        {
            data.length === 0 ?
                <EmptyList
                    code={ emptyLabelCode }
                /> :
                <FlatList
                    data={ data }
                    renderItem={ ({ item, index }) => renderItem({
                        item,
                        index,
                        renderSuffix,
                        onItemPress,
                    }) }
                    ListEmptyComponent={ EmptyList }
                    keyExtractor={ (item, index) => index }
                />
        }
    </View>
);

const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        marginHorizontal: -25,
        marginVertical: 10,
    },

    emptyList: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 20,
        opacity: .7,
    },

    label: {
        fontFamily: 'underdog',
        fontSize: 18,
        marginVertical: 20,
        textAlign: 'center',
        color: COLORS.grey,
    },
});
