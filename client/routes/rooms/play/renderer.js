import React from 'react';
import {
    View,
    Text,
    StyleSheet,
} from 'react-native';

import { Redirect } from 'react-router-native';

import Header from 'client/ui-kit/header';
import Button from 'client/ui-kit/button';
import Counter from 'client/ui-kit/counter';

import PlayersList from 'client/modules/playersList';

import I18n from 'client/i18n';

import { COLORS } from 'client/ui-kit/constants';

const PlayersListSuffix = ({ player: item }) => (
    <View style={ styles.listItemStats }>
        <Text style={[
            styles.label,
            styles.statLabel,
            item.isCurrentPlayer ? styles.labelActive : null,
            item.level === 9 ? styles.labelActiveWarning : null,
        ]}>
            { item.level }
        </Text>
        <Text style={ styles.separator }>+</Text>
        <Text style={[
            styles.label,
            styles.statLabel,
            item.isCurrentPlayer ? styles.labelActive : null,
        ]}>
            { item.bonuses }
        </Text>
        <Text style={ styles.separator }>=</Text>
        <Text style={[
            styles.label,
            styles.statLabel,
            styles.statTotalLabel,
            item.isCurrentPlayer ? styles.labelActive : null,
        ]}>
            { item.level + item.bonuses }
        </Text>
    </View>
);

export default ({
    data: {
        name,
        players,
        currentPlayerIndex,
        status,
        fight,
    },

    selectPlayer,
    stopGame,
    startFight,
    finishTurn,
    incrementPlayerLevel,
    decrementPlayerLevel,
    incrementPlayerBonus,
    decrementPlayerBonus,
}) => (
    status === 'pending' ?
        <Redirect to="/rooms/edit/" /> :
        fight.status === 'active' ?
            <Redirect to="/rooms/fight/" /> :
            <View style={ styles.page }>
                <View style={ styles.header }>
                    <View>
                        <Header level={ 4 }>
                            { I18n.t('game.title') }
                        </Header>
                        <Header>
                            { name }
                        </Header>
                    </View>
                    <Button
                        onPress={ stopGame }
                        icon={{
                            family: 'MaterialCommunityIcons',
                            name: 'undo',
                            size: 24,
                            color: COLORS.mustarderBg,
                        }}
                        style={ styles.button }
                    />
                </View>
                <PlayersList
                    players={ players }
                    renderSuffix={ PlayersListSuffix }
                    onItemPress={ selectPlayer }
                />
                <View>
                    <Button
                        titleCode="game.nextPlayer"
                        type="primary"
                        onPress={ finishTurn }
                    />
                    <View style={ styles.controls }>
                        <Counter
                            labelCode="game.level"
                            value={ players[currentPlayerIndex].level }
                            onPressUp={ incrementPlayerLevel }
                            onPressDown={ decrementPlayerLevel }
                        />
                        <View>
                            <Button
                                type="primary"
                                icon={{
                                    family: 'MaterialCommunityIcons',
                                    name: 'sword-cross',
                                    color: COLORS.mustardBg,
                                    size: 40,
                                }}
                                style={ styles.controlButton }
                                onPress={ startFight }
                            />
                        </View>
                        <Counter
                            labelCode="game.bonus"
                            value={ players[currentPlayerIndex].bonuses }
                            onPressUp={ incrementPlayerBonus }
                            onPressDown={ decrementPlayerBonus }
                        />
                    </View>
                </View>
            </View>
);

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },

    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    button: {
        padding: 0,
    },

    label: {
        fontFamily: 'underdog',
        color: COLORS.mustardBg,
    },

    labelActive: {
        color: COLORS.yellowBg,
    },

    labelActiveWarning: {
        fontSize: 28,
    },

    listItemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 4,
        paddingHorizontal: 25,
    },

    nameContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },

    playerName: {
        fontSize: 16,
        marginRight: 10,
    },

    controls: {
        marginTop: 10,
        margin: -25,
        paddingVertical: 10,
        paddingHorizontal: 25,

        flexDirection: 'row',
        alignItems: 'center',
    },

    controlButton: {
        width: 80,
        height: 80,
        borderRadius: 999,
        justifyContent: 'center',
        alignItems: 'center',
    },

    controlValue: {
        paddingTop: 10,
        fontSize: 28,
    },

    controlValueLabel: {
        paddingBottom: 10,
        fontSize: 12,
    },

    listItemStats: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    statLabel: {
        fontSize: 22,
    },

    statTotalLabel: {
        fontSize: 28,
    },

    separator: {
        opacity: .8,
        paddingHorizontal: 10,
        fontFamily: 'underdog',
        color: COLORS.darkBrownFg,
        fontSize: 14,
    },
});
