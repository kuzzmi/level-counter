import React from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';

import Header from '../../../ui-kit/header';
import BackButton from '../../../ui-kit/backButton';
import Button from '../../../ui-kit/button';
import Input from '../../../ui-kit/input';

import RoomsList from '../../../modules/roomsList';

import I18n from '../../../i18n';

export default ({
    data,
    loading,
    query,
    updateQuery,
    search,
    join,
}) => (
    <View style={ styles.page }>
        <View style={ styles.header }>
            <BackButton />
            <View>
                <Header>
                    { I18n.t('rooms.search.title') }
                </Header>
                <Header level={ 5 }>
                    { I18n.t('rooms.search.description') }
                </Header>
            </View>
        </View>
        <View style={ styles.body }>
            <Input
                placeholderCode="rooms.search.query"
                value={ query }
                onChange={ updateQuery }
            />
            <Button
                type="primary"
                titleCode="rooms.search.search"
                onPress={ search }
            />
            <RoomsList
                rooms={ data }
                loading={ loading }
                notAskedLabelCode="rooms.search.notAsked"
                emptyLabelCode="rooms.search.emptyList"
                onPress={ join }
            />
        </View>
    </View>
);

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },

    header: {
        flexDirection: 'row',
    },

    body: {
        flex: 1,
        marginTop: 10,
    },
});
