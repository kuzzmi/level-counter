import React from 'react';
import {
    Text,
} from 'react-native';

export default ({ data }) => (
    <Text>
        { JSON.stringify(data) }
    </Text>
);
