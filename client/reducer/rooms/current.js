import { push } from 'react-router-redux';

export const initialState = {
    data: null,
    loading: false,
    error: false,

    addingNewPlayer: false,
    newPlayerName: '',
    newPlayerGender: '',
};

export const types = {
    ADDING_NEW_PLAYER: 'GAME_ADD_NEW_PLAYER',
    UPDATE_NEW_PLAYER_NAME: 'GAME_NEW_PLAYER_UPDATE_NAME',
    UPDATE_NEW_PLAYER_GENDER: 'GAME_NEW_PLAYER_UPDATE_GENDER',
    ADD_NEW_PLAYER: 'ADD_NEW_PLAYER',
    REMOVE_PLAYER: 'REMOVE_PLAYER',
    UPDATE_TURN: 'UPDATE_TURN',
    WS_SELECT_PLAYER: '@WS/SELECT_PLAYER',
    WS_UPDATE_PLAYER: '@WS/UPDATE_PLAYER',
    WS_UPDATE_FIGHTER: '@WS/UPDATE_FIGHTER',
    WS_UPDATE_MONSTER: '@WS/UPDATE_MONSTER',
    UPDATE_PLAYER: 'UPDATE_PLAYER',
    UPDATE_ROOM_STATUS: 'UPDATE_ROOM_STATUS',
    UPDATE_FIGHT: 'UPDATE_FIGHT',
    CONNECT_TO_ROOM: 'CONNECT_TO_ROOM',
    LOAD_ROOM: 'LOAD_ROOM',
};

export const actions = {
    incrementPlayerLevel: () => ({
        type: types.WS_UPDATE_PLAYER,
        payload: {
            field: 'level',
            value: 1,
        },
    }),

    decrementPlayerLevel: () => ({
        type: types.WS_UPDATE_PLAYER,
        payload: {
            field: 'level',
            value: -1,
        },
    }),

    incrementPlayerBonus: value => ({
        type: types.WS_UPDATE_PLAYER,
        payload: {
            field: 'bonuses',
            value,
        },
    }),

    decrementPlayerBonus: value => ({
        type: types.WS_UPDATE_PLAYER,
        payload: {
            field: 'bonuses',
            value: -1 * value,
        },
    }),

    updatePlayerGender: value => ({
        type: types.WS_UPDATE_PLAYER,
        payload: {
            field: 'gender',
            value,
        },
    }),

    updateNewPlayerName: value => ({
        type: types.UPDATE_NEW_PLAYER_NAME,
        payload: {
            value,
        },
    }),

    updateNewPlayerGender: value => ({
        type: types.UPDATE_NEW_PLAYER_GENDER,
        payload: {
            value,
        },
    }),

    incrementFighterModifiers: value => ({
        type: types.WS_UPDATE_FIGHTER,
        payload: {
            field: 'modifiers',
            value,
        },
    }),

    decrementFighterModifiers: value => ({
        type: types.WS_UPDATE_FIGHTER,
        payload: {
            field: 'modifiers',
            value: -1 * value,
        },
    }),

    incrementMonsterModifiers: value => ({
        type: types.WS_UPDATE_MONSTER,
        payload: {
            field: 'modifiers',
            value,
        },
    }),

    decrementMonsterModifiers: value => ({
        type: types.WS_UPDATE_MONSTER,
        payload: {
            field: 'modifiers',
            value: -1 * value,
        },
    }),

    incrementMonsterLevel: value => ({
        type: types.WS_UPDATE_MONSTER,
        payload: {
            field: 'level',
            value,
        },
    }),

    decrementMonsterLevel: value => ({
        type: types.WS_UPDATE_MONSTER,
        payload: {
            field: 'level',
            value: -1 * value,
        },
    }),

    addNewPlayer: () => ({
        type: types.ADDING_NEW_PLAYER,
        payload: {
            value: true,
        },
    }),

    cancelNewPlayer: () => ({
        type: types.ADDING_NEW_PLAYER,
        payload: {
            value: false,
        },
    }),

    removePlayer: player => dispatch => {
        dispatch({
            type: '@WS/REMOVE_PLAYER',
            payload: {
                player,
            },
        });
    },

    startGame: () => dispatch => {
        dispatch({
            type: '@WS/UPDATE_ROOM_STATUS',
            payload: {
                status: 'active',
            },
        });

        dispatch(push('/rooms/play/'));
    },

    startFight: () => dispatch => {
        dispatch({
            type: '@WS/UPDATE_FIGHT_STATUS',
            payload: {
                status: 'active',
            },
        });

        setTimeout(function() {
            dispatch(push('/rooms/fight/'));
        }, 200);
    },

    stopFight: () => dispatch => {
        dispatch({
            type: '@WS/UPDATE_FIGHT_STATUS',
            payload: {
                status: 'inactive',
            },
        });

        setTimeout(function() {
            dispatch(push('/rooms/play/'));
        }, 200);
    },

    stopGame: () => ({
        type: '@WS/UPDATE_ROOM_STATUS',
        payload: {
            status: 'pending',
        },
    }),

    saveNewPlayer: () => (dispatch, getState) => {
        const { current } = getState().rooms;
        const player = {
            name: current.newPlayerName,
            gender: current.newPlayerGender,
        };

        dispatch({
            type: '@WS/ADD_NEW_PLAYER',
            payload: {
                player,
            },
        });
    },

    finishTurn: () => ({
        type: '@WS/NEXT_TURN',
    }),

    selectPlayer: index => ({
        type: types.WS_SELECT_PLAYER,
        payload: {
            index,
        },
    }),

    load: () => ({
        type: types.LOAD_ROOM,
    }),
};

const handlers = {
    [types.UPDATE_TURN]: (state, newIndex) => {
        const { data, loading, error } = state;

        if (loading || error) {
            return state;
        }

        return {
            ...state,
            data: {
                ...data,
                currentPlayerIndex: newIndex,
            },
        };
    },

    [types.UPDATE_PLAYER]: (state, newPlayer) => {
        const { data, loading, error } = state;

        if (loading || error) {
            return state;
        }

        const { players, currentPlayerIndex } = data;

        if (players.length === 0) {
            return state;
        }

        const players_ = players.map((player, index) => {
            if (index === currentPlayerIndex) {
                return newPlayer;
            } else {
                return player;
            }
        });

        return {
            ...state,
            data: {
                ...data,
                players: players_,
            },
        };
    },

    [types.REMOVE_PLAYER]: (state, playerId) => {
        const { data, loading, error } = state;

        if (loading || error) {
            return state;
        }

        return {
            ...state,
            data: {
                ...data,
                players: data.players.filter(p => p._id !== playerId),
            },
        };
    },

    [types.ADDING_NEW_PLAYER]: (state, { value }) => ({
        ...state,
        addingNewPlayer: value,
        newPlayerName: '',
        newPlayerGender: '',
    }),

    [types.UPDATE_NEW_PLAYER_NAME]: (state, { value }) => ({
        ...state,
        newPlayerName: value,
    }),

    [types.UPDATE_NEW_PLAYER_GENDER]: (state, { value }) => ({
        ...state,
        newPlayerGender: value,
    }),

    [types.UPDATE_ROOM_STATUS]: (state, status) => {
        const { data, loading, error } = state;

        if (loading || error) {
            return state;
        }

        return {
            ...state,
            data: {
                ...data,
                status,
                players: data.players.map(p => ({
                    ...p,
                    level: 1,
                    bonuses: 0,
                })),
            },
        };
    },

    [types.UPDATE_FIGHT]: (state, fight) => {
        const { data, loading, error } = state;

        if (loading || error) {
            return state;
        }

        console.log(fight);

        return {
            ...state,
            data: {
                ...data,
                fight,
            },
        };
    },

    [types.ADD_NEW_PLAYER]: (state, player) => {
        const { data, loading, error } = state;

        if (loading || error) {
            return state;
        }

        return {
            ...state,
            data: {
                ...data,
                players: [
                    ...data.players,
                    player,
                ],
            },

            addingNewPlayer: false,
            newPlayerName: '',
            newPlayerGender: '',
        };
    },

    [types.CONNECT_TO_ROOM]: (state, room) => ({
        ...state,
        data: room,
        loading: false,
        error: false,
    }),

    [types.LOAD_ROOM]: state => ({
        ...state,
        data: null,
        loading: true,
        error: false,
    }),
};

export default (state = initialState, { type, payload }) => {
    const handler = handlers[type];

    const state_ = handler ? handler(state, payload) : state;

    return state_;
};
