import { actions as roomsActions } from './index.js';

export const types = {
    UPDATE_NAME: 'ROOM_CREATE_UPDATE_NAME',
    UPDATE_PASSWORD: 'ROOM_CREATE_UPDATE_PASSWORD',
    SAVING: 'ROOM_CREATE_SAVING',
    SUCCESS: 'ROOM_CREATE_SUCCESS',
    FAIL: 'ROOM_CREATE_FAIL',
};

export const actions = {
    updateName: value => ({
        type: types.UPDATE_NAME,
        payload: {
            value,
        },
    }),

    updatePassword: value => ({
        type: types.UPDATE_PASSWORD,
        payload: {
            value,
        },
    }),

    save: () => (dispatch, getState, api) => {
        const { rooms, profile } = getState();
        const { name, password } = rooms.create;

        dispatch({
            type: types.SAVING,
        });

        const saveSuccess = room => {
            dispatch({
                type: types.SUCCESS,
                payload: {
                    room,
                },
            });

            dispatch(roomsActions.join(room));

            return room;
        };

        const saveFailure = error => {
            dispatch({
                type: types.FAIL,
                payload: {
                    error,
                },
            });
        };

        return api.rooms
            .create({ name, password, profile })
            .then(saveSuccess, saveFailure)
            .catch(saveFailure);
    },
};

const initialState = {
    name: '',
    password: '',

    saving: false,
    data: null,
    error: false,
};

const handlers = {
    [types.UPDATE_NAME]: (state, { value }) => ({
        ...state,
        name: value,
    }),

    [types.UPDATE_PASSWORD]: (state, { value }) => ({
        ...state,
        password: value,
    }),

    [types.SAVING]: state => ({
        ...state,
        saving: true,
        data: null,
        error: false,
    }),

    [types.SUCCESS]: (state, { room }) => ({
        ...state,
        saving: false,
        data: room,
        error: false,
    }),

    [types.FAIL]: (state, { error }) => ({
        ...state,
        saving: false,
        data: null,
        error,
    }),
};

export default (state = initialState, { type, payload }) => {
    const handler = handlers[type];

    const state_ = handler ? handler(state, payload) : state;

    return state_;
};
