export const COLORS = {
    yellowBg: '#32343E',
    darkerBrownFg: '#4A4E58',
    darkBrownFg: '#7E838E',
    mustardBg: '#FDDD75',
    mustarderBg: '#FDE475',
    grey: '#D8D3C8',
    white: '#FFFFFF',
};
