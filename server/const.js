module.exports.ROLE = {
    GM: 'gm',
    PLAYER: 'player',
    MONSTER: 'monster',
};

module.exports.GENDER = {
    MALE: 'male',
    FEMALE: 'female',
};

module.exports.ROOM_STATUS = {
    ACTIVE: 'active',
    PENDING: 'pending',
};

module.exports.FIGHT_STATUS = {
    ACTIVE: 'active',
    INACTIVE: 'inactive',
};
