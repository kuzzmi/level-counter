export const types = {
    REQUEST: 'ROOMS_SEARCH_REQUEST',
    FAILED: 'ROOMS_SEARCH_FAILED',
    SUCCESS: 'ROOMS_SEARCH_SUCCESS',
    UPDATE_QUERY: 'ROOMS_SEARCH_UPDATE_QUERY',
};

export const actions = {
    updateQuery: value => ({
        type: types.UPDATE_QUERY,
        payload: {
            value,
        },
    }),

    search: () => (dispatch, getState, api) => {
        const query = getState().rooms.search.query;

        dispatch({
            type: types.REQUEST,
        });

        const success = data =>
            dispatch({
                type: types.SUCCESS,
                payload: {
                    data,
                },
            });

        const failed = error =>
            dispatch({
                type: types.FAILED,
                payload: {
                    error,
                },
            });

        return api.rooms.search({ query })
            .then(success, failed)
            .catch(failed);
    },
};

export const initialState = {
    query: 'room',

    data: null,
    error: false,
    loading: false,
};

const handlers = {
    [types.UPDATE_QUERY]: (state, { value }) => ({
        ...state,
        query: value,
    }),

    [types.REQUEST]: state => ({
        ...state,
        loading: true,
        error: false,
        data: false,
    }),

    [types.FAILED]: (state, { error }) => ({
        ...state,
        error,
        data: null,
        loading: false,
    }),

    [types.SUCCESS]: (state, { data }) => ({
        ...state,
        data,
        loading: false,
        error: false,
    }),
};

export default (state = initialState, { type, payload }) => {
    const handler = handlers[type];

    const state_ = handler ? handler(state, payload) : state;

    return state_;
};
