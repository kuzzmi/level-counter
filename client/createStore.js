import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import uuid from 'uuid/v1';

// history
import createHistory from 'history/createMemoryHistory';
export const history = createHistory();

import API from './api.js';

import reducer from './reducer';

import { actions as profileActions } from 'client/reducer/profile';
import { actions as historyActions } from 'client/reducer/rooms/history';

import createWsMiddleware from './createWsMiddleware.js';

export default function() {
    const store = createStore(
        reducer,
        applyMiddleware(
            thunkMiddleware.withExtraArgument(API),
            routerMiddleware(history),
            createWsMiddleware(`ws://192.168.0.122:3311/?uuid=${ uuid() }`),
        )
    );

    // initial actions
    store.dispatch(profileActions.request());
    store.dispatch(historyActions.request());

    return store;
}
