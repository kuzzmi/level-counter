import { connect } from 'react-redux';
import Renderer from './renderer';
import { actions } from 'client/reducer/rooms/search';
import { actions as roomsActions } from 'client/reducer/rooms';

const mapStateToProps = ({ rooms: { search } }) => search;

const mapDispatchToProps = ({
    ...actions,
    ...roomsActions,
});

export default connect(mapStateToProps, mapDispatchToProps)(Renderer);
