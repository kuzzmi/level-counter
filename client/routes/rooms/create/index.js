import { connect } from 'react-redux';
import Renderer from './renderer';
import { actions } from 'client/reducer/rooms/create';

const mapStateToProps = ({ rooms: { create }, profile }) => ({
    ...create,
    profile,
});

const mapDispatchToProps = actions;

export default connect(mapStateToProps, mapDispatchToProps)(Renderer);
